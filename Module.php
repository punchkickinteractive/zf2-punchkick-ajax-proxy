<?php

namespace Punchkick\AjaxProxy;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Http\Request as HttpRequest;
use Zend\Http\Response as HttpResponse;
use Zend\Uri\Http as Uri;
use Zend\Stdlib\Parameters;

class Module implements BootstrapListenerInterface, ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    public function onBootstrap(EventInterface $event)
    {

        $request    = $event->getRequest();
        $config     = $event->getApplication()->getConfig();
        $config     = $config['punchkick-ajax-proxy'];

        if (!$request instanceof HttpRequest) {
            return;
        }

        // only run on the registered path for the module
        if ($request->getUri()->getPath() !== $config['path']) {
            return;
        }

        // only run on POST/OPTIONS
        if ($request->getMethod() !== 'OPTIONS' && $request->getMethod() !== 'POST') {
            return;
        }

        // inject headers for CORs
        $this->injectPreflight($request);

        // if the request was on OPTIONS request, don't allow
        // further processing of the event --- would end up in a 404
        if ($request->getMethod() === 'OPTIONS') {
            exit;
        }

        $settings = $this->getData($request);

        // check the for the URL data, if it's there conver it to a Uri
        // so we can easily compare it to the incoming Uri
        if (isset($settings['url'])) {

            $settings['url'] = new Uri($settings['url']);
            // if the request host is different than the host of the url passed,
            // we're a proxy!
            if ($request->getUri()->getHost() !== $settings['url']->getHost()) {
                // it's an external reqeust!
                $this->proxy($request, $settings);
                return;
            }
        }
        // otherwise, we just alter the request
        $this->alter($request, $settings);
    }

    private function proxy($request, $settings)
    {
        // @TODO
        exit('External proxies not yet implemented.');
    }


    private function alter(HttpRequest $request, Array $settings)
    {

        /**
         * Override the url
         */
        if (isset($settings['url'])) {
            $request->setUri($settings['url']);
        }

        /**
         * Override the method
         */
        if (isset($settings['type'])) {
            $request->setMethod($settings['type']);
        }

        /**
         * Override the content/query
         */
        if (isset($settings['data'])) {
            if ($request->getMethod() === 'GET') {
                $request->getUri()->setQuery($settings['data']);
            }

            $request->setContent($settings['data']);
        }

        // merge the request headers with the
        // setting headers and the "default" headers
        $headers = array(
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json'
        );
        if (isset($settings['headers'])) {
            $headers = array_merge(
                    $headers,
                    $settings['headers'],
                    $request->getHeaders()->toArray()
            );
        }

        $request->getHeaders()->addHeaders($headers);

        return $request;
    }

    private function getData(HttpRequest $request)
    {
        // check the raw content first
        $data = json_decode($request->getContent(), true);

        // if the data wasn't sent raw, get it from the POST
        if (empty($data)) {
            $data = $request->getPost()->toArray();
        }
        return $data;
    }

    // @TODO -- move to config, maybe use Zend Response
    private function injectPreflight(HttpRequest $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: X-Auth-Token, Content-Type, Accept');
    }
}
