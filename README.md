##Punchkick Ajax Proxy

Allow request proxying via an API endpoint.

Makes CORs using all HTTP methods possible for older browsers via a simple `POST` method.

### Configuration

All configuration values should live under the `punchkick-ajax-proxy` key.

```
return array(
    'punchkick-ajax-proxy' => array(
        'path' => '/proxy'
    )
);

```

#### `path` 

`string` The exact path which the module will listen on. This will be the path that accepts proxy requests.

### Usage

By `POST`ing  jQuery `.ajax` settings to the registered endpoint, the in-bound request will be altered or sent via proxy (TODO) to the desired endpoint with provided properties.

*EXAMPLE*

```
    POST http://example.api.com/proxy
    { "url": "http://example.api.com/items", "type": "GET", "headers" : { "X-Auth-Token": "30fbbe54753e33b116623ac93950a569_487722d311862fbae6f1af1bfbbde9a3a35f1eed" } }
```

This request payload would result in a `GET` request being made to the provided `url` injected with the provided `headers`. The response to this request would be identical to
requesting the payload's `url` with the provided settings directly.


```
200 OK
{
    "meta": {
        "pagination": {
            "page_current": 1,
            "page_total": 1,
            "page_size": 100,
            "total": 6,
            "count": 6
        }
    },
    "total": 30798,
    "items": [
        {
        ...
```